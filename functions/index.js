const functions = require('firebase-functions');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const MongoClient = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectId;
const url = 'mongodb://admin:$uFrlk$qeS=l+7a#w9Tr@alzaheroes-shard-00-00-l6c1o.mongodb.net:27017,alzaheroes-shard-00-01-l6c1o.mongodb.net:27017,alzaheroes-shard-00-02-l6c1o.mongodb.net:27017/test?ssl=true&replicaSet=AlzaHeroes-shard-0&authSource=admin&retryWrites=true&w=majority';

// Express
const app = express();
app.use(cors({ origin: true })); // Automatically allow cross-origin requests
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // Support json encoded bodies

/**
 * MongoDB: Establish and cache DB connection
*/
let mongoClient;

const mongoConnect = async () => {
  // Connect only when connection isn't alive
  if (!(mongoClient && mongoClient.isConnected())) {
    try {
      mongoClient = await MongoClient.connect(url, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
      });
    }
    catch (e) { throw e; }
  }

  return mongoClient.db("alza-heroes");
}

/**
 * Get Heroes
 * Returns list of heroes
 */
app.get('/heroes', async (req, res) => {
  const db = await mongoConnect();

  db.collection("heroes").find({}).toArray(function (err, result) {
    if (err) res.status(400).send(err);
    res.status(200).send(result);
  });
});

/**
 * Get Hero
 * Returns single hero
 */
app.get('/heroes/:id', async (req, res) => {
  const db = await mongoConnect();

  db.collection("heroes").findOne({ "_id": ObjectId(req.params.id) }, function (err, result) {
    if (err) res.status(400).send(err);
    res.status(200).send(result);
  });
});

/**
 * Create Hero
 */
app.post('/heroes', async (req, res) => {
  const db = await mongoConnect();
  const newHero = { name: req.body.name };

  // Check for empty name
  if (!req.body || !req.body.name || !req.body.name === '')
    return res.status(400).send({message: 'Error: Property name is required.'});

  db.collection("heroes").insertOne(newHero, function (err, result) {
    if (err) res.status(400).send({err, message: 'Error while inserting hero into database.'});

    return res.status(200).send(result.ops[0]);
  });
});

/**
 * Update Hero
 */
app.put('/heroes/:id', async (req, res) => {
  const db = await mongoConnect();
  const updatedHero = { name: req.body.name };

  db.collection("heroes").findOneAndUpdate(
    { "_id": ObjectId(req.params.id) },
    { $set: updatedHero },
    { returnOriginal: false },

    function (err, result) {
      if (err) res.status(400).send(err);
      res.status(200).send(result.value);
    });
});

/**
 * Delete Hero
 */
app.delete('/heroes/:id', async (req, res) => {
  const db = await mongoConnect();

  db.collection("heroes").deleteOne({ "_id": ObjectId(req.params.id) }, function (err, result) {
    if (err) res.status(400).send(err);
    res.status(200).send(result);
  });
});

exports.api = functions.region('europe-west1').https.onRequest(app);
