export const environment = {
  production: true,
  apiUrl: 'https://europe-west1-alza-heroes.cloudfunctions.net/api'
};
