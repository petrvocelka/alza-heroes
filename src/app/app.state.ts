import { IHeroesState } from './shared/interfaces/IHeroesState';
import { IHeroDetailState } from './shared/interfaces/IHeroDetailState';

export interface AppState {
  readonly heroes: IHeroesState;
  readonly heroDetail: IHeroDetailState;
}
