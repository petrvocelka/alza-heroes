import { Action } from '@ngrx/store';
import { IHero } from '../../shared/interfaces/IHero';

export const GET_HEROES = 'GET_HEROES';
export const GET_HEROES_SUCCESS = 'GET_HEROES_SUCCESS';
export const GET_HEROES_ERROR = 'GET_HEROES_ERROR';

export const CREATE_HERO = 'CREATE_HERO';
export const CREATE_HERO_SUCCESS = 'CREATE_HERO_SUCCESS';
export const CREATE_HERO_ERROR = 'CREATE_HERO_ERROR';

export const GET_HERO = 'GET_HERO';
export const GET_HERO_SUCCESS = 'GET_HERO_SUCCESS';
export const GET_HERO_ERROR = 'GET_HERO_ERROR';

export const REMOVE_HERO = 'REMOVE_HERO';
export const REMOVE_HERO_SUCCESS = 'REMOVE_HERO_SUCCESS';
export const REMOVE_HERO_ERROR = 'REMOVE_HERO_ERROR';

export const UPDATE_HERO = 'UPDATE_HERO';
export const UPDATE_HERO_SUCCESS = 'UPDATE_HERO_SUCCESS';
export const UPDATE_HERO_ERROR = 'UPDATE_HERO_ERROR';

export const SELECT_HERO = 'SELECT_HERO';

// Get Heroes (list)
export class GetHeroes implements Action {
  readonly type = GET_HEROES;
}
export class GetHeroesSuccess implements Action {
  readonly type = GET_HEROES_SUCCESS;
  constructor(public payload: IHero[]) {}
}
export class GetHeroesError implements Action {
  readonly type = GET_HEROES_ERROR;
}

// Add Hero
export class AddHero implements Action {
  readonly type = CREATE_HERO;
  constructor(public name: string) {}
}
export class AddHeroSuccess implements Action {
  readonly type = CREATE_HERO_SUCCESS;
  constructor(public payload: IHero) {}
}
export class AddHeroError implements Action {
  readonly type = CREATE_HERO_ERROR;
}

// Get Hero (detail)
export class GetHero implements Action {
  readonly type = GET_HERO;
  constructor(public id: string) {}
}
export class GetHeroSuccess implements Action {
  readonly type = GET_HERO_SUCCESS;
  constructor(public payload: IHero) {}
}
export class GetHeroError implements Action {
  readonly type = GET_HERO_ERROR;
}

// Remove Hero
export class RemoveHero implements Action {
  readonly type = REMOVE_HERO;
  constructor(public id: string) {}
}
export class RemoveHeroSuccess implements Action {
  readonly type = REMOVE_HERO_SUCCESS;
  constructor(public payload: string) {}
}
export class RemoveHeroError implements Action {
  readonly type = REMOVE_HERO_ERROR;
}

// Update Hero
export class UpdateHero implements Action {
  readonly type = UPDATE_HERO;
  constructor(public hero: IHero) {}
}
export class UpdateHeroSuccess implements Action {
  readonly type = UPDATE_HERO_SUCCESS;
  constructor(public payload: IHero) {}
}
export class UpdateHeroError implements Action {
  readonly type = UPDATE_HERO_ERROR;
}

// Select Hero
export class SelectHero implements Action {
  readonly type = SELECT_HERO;
  constructor(public payload: IHero) {}
}

export type Actions =
    GetHeroes
  | GetHeroesSuccess
  | GetHeroesError
  | AddHero
  | AddHeroSuccess
  | AddHeroError
  | GetHero
  | GetHeroSuccess
  | GetHeroError
  | RemoveHero
  | RemoveHeroSuccess
  | RemoveHeroError
  | UpdateHero
  | UpdateHeroSuccess
  | UpdateHeroError
  | SelectHero
;
