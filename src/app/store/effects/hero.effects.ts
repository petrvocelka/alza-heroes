import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { of } from 'rxjs';
import { HeroService } from '../../shared/services/hero.service';
import {
  GET_HEROES, GET_HEROES_SUCCESS, GET_HEROES_ERROR,
  CREATE_HERO, CREATE_HERO_SUCCESS, CREATE_HERO_ERROR,
  GET_HERO, GET_HERO_SUCCESS, GET_HERO_ERROR,
  REMOVE_HERO, REMOVE_HERO_SUCCESS, REMOVE_HERO_ERROR,
  UPDATE_HERO, UPDATE_HERO_SUCCESS, UPDATE_HERO_ERROR,
} from 'store/actions/hero.actions';

import { switchMap, map, catchError } from 'rxjs/operators';
import { IHero } from 'app/shared/interfaces/IHero';

@Injectable()
export class HeroEffects {
  constructor(
    private actions: Actions,
    private heroService: HeroService,
    private toastr: ToastrService,
  ) {}

  @Effect() getHero = this.actions.pipe(
    ofType(GET_HERO),
    switchMap((action: any) =>
      this.heroService.getHero(action.id).pipe(
        map(hero => ({type: GET_HERO_SUCCESS, payload: hero})),
        catchError(() => of({type: GET_HERO_ERROR})))
      )
  );

  @Effect() createHero = this.actions.pipe(
    ofType(CREATE_HERO),
    switchMap((action: any) =>
      this.heroService.createHero(action.name).pipe(
        map((hero: IHero) => {
          this.toastr.success(`Hero ${hero.name} successfuly created.`, '', { timeOut: 2000 });
          return ({type: CREATE_HERO_SUCCESS, payload: hero});
        }),
        catchError(() => {
          this.toastr.error('Error', 'Unable to create hero', { timeOut: 2000 });
          return of({type: CREATE_HERO_ERROR});
        })
      ))
  );

  @Effect() updateHero = this.actions.pipe(
    ofType(UPDATE_HERO),
    switchMap((action: any) =>
      this.heroService.updateHero(action.hero).pipe(
        map((hero: IHero) => {
          this.toastr.success(`Hero ${hero.name} successfuly updated.`, '', { timeOut: 2000 });
          return ({type: UPDATE_HERO_SUCCESS, payload: hero});
        }),
        catchError(() => {
          this.toastr.error('Error', 'Unable to update hero', { timeOut: 2000 });
          return of({type: UPDATE_HERO_ERROR});
        })
      ))
  );

  @Effect() removeHero = this.actions.pipe(
    ofType(REMOVE_HERO),
    switchMap((action: any) =>
      this.heroService.removeHero(action.id).pipe(
        map(() => ({type: REMOVE_HERO_SUCCESS, payload: action.id})),
        catchError(() => {
            this.toastr.error('Error', 'Unable to remove hero', { timeOut: 2000 });
            return of({type: REMOVE_HERO_ERROR});
          }
        ))
      )
  );

  @Effect() getHeroes = this.actions.pipe(
    ofType(GET_HEROES),
    switchMap(() =>
      this.heroService.getHeroes().pipe(
        map(heroes => ({type: GET_HEROES_SUCCESS, payload: heroes})),
        catchError(() => of({type: GET_HEROES_ERROR})))
      )
  );
}
