import * as heroesActions from '../actions/hero.actions';
import { IHeroDetailState } from 'app/shared/interfaces/IHeroDetailState';

// Initial Heroes State
const initialState: IHeroDetailState = {
  data: null,
  network: {
    pending: false,
    error: null,
  },
  formUpdateHero: {
    pending: false,
  },
};

export function heroDetailReducer(prevState = initialState, action: heroesActions.Actions) {
  const state = { ...prevState };

  switch (action.type) {
    // Get Hero (detail)
    case heroesActions.GET_HERO:
      state.network = { pending: true, error: null };
      break;

    case heroesActions.GET_HERO_SUCCESS:
      state.data = action.payload,
      state.network = { pending: false, error: null };
      break;

    case heroesActions.GET_HERO_ERROR:
      state.network = { pending: false, error: 'Error: Unable to load hero.' };
      break;

    // Update Hero
    case heroesActions.UPDATE_HERO:
      state.formUpdateHero = { pending: true };
      break;

    case heroesActions.UPDATE_HERO_SUCCESS:
      state.data = action.payload;
      state.formUpdateHero = { pending: false };
      break;

    case heroesActions.UPDATE_HERO_ERROR:
      state.formUpdateHero = { pending: false };
      break;
  }

  return state;
}
