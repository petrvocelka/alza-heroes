import { IHero } from '../../shared/interfaces/IHero';
import * as heroesActions from '../actions/hero.actions';
import { IHeroesState } from 'app/shared/interfaces/IHeroesState';

// Initial Heroes State
const initialState: IHeroesState = {
  data: [],
  selectedHero: null,
  network: {
    pending: false,
    error: null,
  },
  formCreateHero: {
    loading: false,
  },
  removeHeroButtonLoading: false,
};

export function heroesReducer(prevState = initialState, action: heroesActions.Actions) {
  const state = { ...prevState };

  switch (action.type) {
    // Get Heroes
    case heroesActions.GET_HEROES:
      state.network = { pending: true, error: null };
      break;

    case heroesActions.GET_HEROES_SUCCESS:
      state.data = action.payload;
      state.network = { pending: false, error: null };
      break;

    case heroesActions.GET_HEROES_ERROR:
      state.network = { pending: false, error: 'Error: Unable to load heroes.' };
      break;

    // Add Hero
    case heroesActions.CREATE_HERO:
      state.network = { pending: false, error: null };
      state.formCreateHero = { loading: true };
      break;

    case heroesActions.CREATE_HERO_SUCCESS:
      state.data = [...state.data, action.payload],
      state.formCreateHero = { loading: false };
      state.network = { pending: false, error: null };
      break;

    case heroesActions.CREATE_HERO_ERROR:
      state.formCreateHero = { loading: false };
      state.network = { pending: false, error: 'Error: Unable to add hero.' };
      break;

    // Remove Hero
    case heroesActions.REMOVE_HERO:
      state.removeHeroButtonLoading = true;
      break;

    case heroesActions.REMOVE_HERO_SUCCESS:
      state.data = state.data.filter((hero: IHero) => hero._id !== action.payload);
      state.selectedHero = (state.selectedHero && state.selectedHero._id !== action.payload) ? state.selectedHero : null;
      state.network = { pending: false, error: null };
      state.removeHeroButtonLoading = false;
      break;

    case heroesActions.REMOVE_HERO_ERROR:
      state.network = { pending: false, error: 'Error: Unable to remove hero.' };
      state.removeHeroButtonLoading = false;
      break;

    // Update Hero
    case heroesActions.UPDATE_HERO_SUCCESS:
      if (state.selectedHero && state.selectedHero._id === action.payload._id) {
        state.selectedHero = action.payload;
      }
      break;

    // Select Hero
    case heroesActions.SELECT_HERO:
      state.selectedHero = action.payload;
      break;
  }

  return state;
}
