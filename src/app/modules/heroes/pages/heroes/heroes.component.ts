import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Store } from '@ngrx/store';
import { IHero } from 'shared/interfaces/IHero';
import { AppState } from 'app/app.state';
import * as heroesActions from 'app/store/actions/hero.actions';

@Component({
  selector: 'app-heroes',
  templateUrl: './heroes.component.html',
  styleUrls: ['./heroes.component.scss']
})
export class HeroesComponent implements OnInit {

  heroes$: Observable<any>;

  constructor(private store: Store<AppState>) {
    this.heroes$ = store.select('heroes');
  }

  ngOnInit() {
    this.store.dispatch(new heroesActions.GetHeroes());
  }

  onSelectHero(hero: IHero) {
    this.store.dispatch(new heroesActions.SelectHero(hero));
  }

  onCreateHero(heroName: string) {
    this.store.dispatch(new heroesActions.AddHero(heroName));
  }

  onRemoveHero(hero: IHero) {
    this.store.dispatch(new heroesActions.RemoveHero(hero._id));
  }
}
