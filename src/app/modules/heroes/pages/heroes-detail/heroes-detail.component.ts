import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { Store } from '@ngrx/store';
import { AppState } from 'app/app.state';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';

import * as heroesActions from 'app/store/actions/hero.actions';

@Component({
  selector: 'app-heroes-detail',
  templateUrl: './heroes-detail.component.html',
  styleUrls: ['./heroes-detail.component.scss']
})

export class HeroesDetailComponent implements OnInit {

  heroID = this.route.snapshot.paramMap.get('id');
  heroDetail$: Observable<any>;

  constructor(
    private location: Location,
    private store: Store<AppState>,
    private route: ActivatedRoute,
  ) {
    this.heroDetail$ = store.select('heroDetail');
  }

  ngOnInit() {
    // Load Heroes from server
    this.store.dispatch(new heroesActions.GetHero(this.heroID));
  }

  goBack(): void {
    this.location.back();
  }

  onUpdateHero(hero) {
    this.store.dispatch(new heroesActions.UpdateHero(hero));
  }
}
