import { Component, OnInit, OnDestroy } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs/internal/Observable';
import { IHero } from 'shared/interfaces/IHero';
import { AppState } from '../../../../app.state';
import * as heroesActions from 'app/store/actions/hero.actions';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
  // Constants
  TOP_HEROES_COUNT = 4;

  // Subscriptions
  heroes$: Observable<any>;

  constructor(private store: Store<AppState>) {
    this.heroes$ = store.select('heroes');
  }

  ngOnInit() {
    this.store.dispatch(new heroesActions.GetHeroes());
  }
}
