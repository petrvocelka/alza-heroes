import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { IFormCreateHero } from 'app/shared/interfaces/IFormCreateHero';

@Component({
  selector: 'app-form-create-hero',
  templateUrl: './form-create-hero.component.html',
  styleUrls: ['./form-create-hero.component.scss']
})
export class FormCreateHeroComponent implements OnInit {

  @Input() form: IFormCreateHero = null;
  @Output() heroCreated = new EventEmitter<string>();

  public formCreateHero = new FormGroup({
    name: new FormControl('', [Validators.required]),
  });

  constructor() {}

  ngOnInit() {}

  onSubmit() {
    if (this.formCreateHero.valid) {
      const heroName: string = this.formCreateHero.value.name;
      this.heroCreated.emit(heroName);
    }
  }
}
