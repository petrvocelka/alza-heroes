import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormCreateHeroComponent } from './form-create-hero.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonSubmitComponent } from 'app/shared/components/button-submit/button-submit.component';
import { SpinnerComponent } from 'app/shared/components/spinner/spinner.component';

describe('FormCreateHeroComponent', () => {
  let component: FormCreateHeroComponent;
  let fixture: ComponentFixture<FormCreateHeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FormCreateHeroComponent,
        ButtonSubmitComponent,
        SpinnerComponent,
      ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormCreateHeroComponent);
    component = fixture.componentInstance;
    component.form = {
      loading: true
    };
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
