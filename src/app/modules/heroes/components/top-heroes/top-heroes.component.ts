import { Component, OnInit, Input } from '@angular/core';
import { IHero } from 'app/shared/interfaces/IHero';


@Component({
  selector: 'app-top-heroes',
  templateUrl: './top-heroes.component.html',
  styleUrls: ['./top-heroes.component.scss']
})
export class TopHeroesComponent implements OnInit {

  @Input() heroes: IHero[] = null;

  constructor() { }

  ngOnInit() {
  }

}
