import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Store } from '@ngrx/store';
import { IHero } from 'shared/interfaces/IHero';
import { AppState } from 'app/app.state';

import * as heroesActions from 'app/store/actions/hero.actions';

@Component({
  selector: 'app-list-heroes',
  templateUrl: './list-heroes.component.html',
  styleUrls: ['./list-heroes.component.scss']
})
export class ListHeroesComponent implements OnInit {

  @Input() heroes: IHero[] = null;
  @Output() selectChanged = new EventEmitter<IHero>();

  constructor() {}

  ngOnInit() {}

  /**
   * Select Hero
   */
  selectHero(hero: IHero) {
    this.selectChanged.emit(hero);
  }
}
