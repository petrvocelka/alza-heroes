import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IHero } from 'app/shared/interfaces/IHero';

@Component({
  selector: 'app-quick-detail-hero',
  templateUrl: './quick-detail-hero.component.html',
  styleUrls: ['./quick-detail-hero.component.scss']
})
export class QuickDetailHeroComponent implements OnInit {

  @Input() hero: IHero = null;
  @Input () removeHeroButtonLoading = false;
  @Output() remove = new EventEmitter<IHero>();

  constructor() { }

  ngOnInit() {}

  removeHero() {
    this.remove.emit(this.hero);
  }
}
