import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { QuickDetailHeroComponent } from './quick-detail-hero.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ButtonSubmitComponent } from 'app/shared/components/button-submit/button-submit.component';
import { SpinnerComponent } from 'app/shared/components/spinner/spinner.component';

describe('QuickDetailHeroComponent', () => {
  let component: QuickDetailHeroComponent;
  let fixture: ComponentFixture<QuickDetailHeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ RouterTestingModule ],
      declarations: [
        QuickDetailHeroComponent,
        ButtonSubmitComponent,
        SpinnerComponent,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickDetailHeroComponent);
    component = fixture.componentInstance;

    component.removeHeroButtonLoading = true;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
