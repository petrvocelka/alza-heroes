import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormUpdateHeroComponent } from './form-update-hero.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonSubmitComponent } from 'app/shared/components/button-submit/button-submit.component';
import { SpinnerComponent } from 'app/shared/components/spinner/spinner.component';

describe('FormUpdateHeroComponent', () => {
  let component: FormUpdateHeroComponent;
  let fixture: ComponentFixture<FormUpdateHeroComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        FormUpdateHeroComponent,
        ButtonSubmitComponent,
        SpinnerComponent,
      ],
      imports: [ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormUpdateHeroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
