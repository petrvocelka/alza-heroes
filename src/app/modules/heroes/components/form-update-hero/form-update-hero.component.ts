import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import { IHero } from 'shared/interfaces/IHero';
import { IFormUpdateHero } from 'app/shared/interfaces/IFormUpdateHero';

@Component({
  selector: 'app-form-update-hero',
  templateUrl: './form-update-hero.component.html',
  styleUrls: ['./form-update-hero.component.scss']
})
export class FormUpdateHeroComponent implements OnInit {

  @Input() hero: IHero = null;
  @Input() form: IFormUpdateHero = null;
  @Output() heroUpdated = new EventEmitter<IHero>();

  formUpdateHero: FormGroup = null;

  constructor() {}

  ngOnInit() {
    if (this.hero !== null) {
      this.formUpdateHero = new FormGroup({
        _id: new FormControl(this.hero._id),
        name: new FormControl(this.hero.name, [Validators.required]),
      });
    }
  }

  onSubmit() {
    if (this.formUpdateHero.valid) {
      const hero: IHero = this.formUpdateHero.value;
      this.heroUpdated.emit(hero);
    }
  }
}
