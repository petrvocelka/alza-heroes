import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { ToastrModule } from 'ngx-toastr';

/* Core */
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

/* Components */
import { SpinnerComponent } from './shared/components/spinner/spinner.component';
import { FormCreateHeroComponent } from './modules/heroes/components/form-create-hero/form-create-hero.component';
import { FormUpdateHeroComponent } from './modules/heroes/components/form-update-hero/form-update-hero.component';
import { ListHeroesComponent } from './modules/heroes/components/list-heroes/list-heroes.component';
import { HeaderComponent } from './core/header/header.component';

/* Pages */
import { DashboardComponent } from './modules/heroes/pages/dashboard/dashboard.component';
import { HeroesComponent } from './modules/heroes/pages/heroes/heroes.component';
import { HeroesDetailComponent } from './modules/heroes/pages/heroes-detail/heroes-detail.component';

/* Effects */
import { HeroEffects } from './store/effects/hero.effects';

/* Reducers */
import { heroesReducer } from './store/reducers/heroes.reducer';
import { heroDetailReducer } from './store/reducers/heroDetail.reducer';
import { QuickDetailHeroComponent } from './modules/heroes/components/quick-detail-hero/quick-detail-hero.component';
import { TopHeroesComponent } from './modules/heroes/components/top-heroes/top-heroes.component';
import { ButtonSubmitComponent } from './shared/components/button-submit/button-submit.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    HeroesComponent,
    HeroesDetailComponent,
    ListHeroesComponent,
    FormCreateHeroComponent,
    FormCreateHeroComponent,
    HeaderComponent,
    SpinnerComponent,
    FormUpdateHeroComponent,
    QuickDetailHeroComponent,
    TopHeroesComponent,
    ButtonSubmitComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    EffectsModule.forRoot([HeroEffects]),
    StoreModule.forRoot({
      heroes: heroesReducer,
      heroDetail: heroDetailReducer,
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
