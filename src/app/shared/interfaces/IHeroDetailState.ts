import { IHero } from './IHero';
import { INetwork } from './INetwork';
import { IFormUpdateHero } from './IFormUpdateHero';

export interface IHeroDetailState {
    data: IHero;
    network: INetwork;
    formUpdateHero: IFormUpdateHero;
}
