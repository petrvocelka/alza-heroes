import { IHero } from './IHero';
import { INetwork } from './INetwork';
import { IFormCreateHero } from './IFormCreateHero';

export interface IHeroesState {
  data: IHero[];
  selectedHero: IHero;
  network: INetwork;
  formCreateHero: IFormCreateHero;
  removeHeroButtonLoading: boolean;
}
