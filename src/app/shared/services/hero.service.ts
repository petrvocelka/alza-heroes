import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { IHero } from '../interfaces/IHero';
import { environment } from 'src/environments/environment';
import { CreateHeroDTO } from '../dtos/CreateHeroDTO';

@Injectable({
  providedIn: 'root'
})
export class HeroService {

  /* API Settings */
  public basePath = 'heroes';

  constructor(private http: HttpClient) {}

  /**
   * Get Hero
   * returns hero's profile data
   */
  getHero(id: string) {
    return this.http.get(`${environment.apiUrl}/${this.basePath}/${id}`);
  }

  /**
   * Remove Hero
   */
  removeHero(id: string) {
    return this.http.delete(`${environment.apiUrl}/${this.basePath}/${id}`);
  }

  /**
   * Create Hero
   */
  createHero(name) {
    const reqUrl = `${environment.apiUrl}/${this.basePath}`;
    const reqBody: CreateHeroDTO = { name };

    return this.http.post(reqUrl, reqBody);
  }

  /**
   * Update Hero
   */
  updateHero(hero: IHero) {
    const reqUrl = `${environment.apiUrl}/${this.basePath}/${hero._id}`;
    const reqBody = hero;

    return this.http.put(reqUrl, reqBody);
  }

  /**
   * Get Heroes
   * returns list of heroes
   */
  getHeroes() {
    return this.http.get(`${environment.apiUrl}/${this.basePath}`);
  }
}
