import { IHero } from '../interfaces/IHero';

export const getHeroMock: IHero = {
  _id: '5e7bef58a6e7c01c792c37c8',
  name: 'Test Hero',
};

export const getHeroesMock: IHero[] = [getHeroMock, getHeroMock, getHeroMock];
export const createHeroMock: IHero = getHeroMock;

