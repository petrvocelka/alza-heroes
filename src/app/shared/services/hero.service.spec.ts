import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HeroService } from './hero.service';
import { IHero } from '../interfaces/IHero';
import { environment } from 'src/environments/environment';
import { getHeroMock, getHeroesMock, createHeroMock } from './hero.service.mock';

describe('HeroService', () => {
  // We declare the variables that we'll use for the Test Controller and for our Service
  let httpTestingController: HttpTestingController;
  let heroService: HeroService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HeroService],
      imports: [HttpClientTestingModule]
    });

    httpTestingController = TestBed.get(HttpTestingController);
    heroService = TestBed.get(HeroService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(heroService).toBeTruthy();
  });
});

describe('HeroService', () => {
  let httpMock: HttpTestingController;
  let heroService: HeroService;

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [ HttpClientTestingModule ],
        providers: [ HeroService ]
    });

    heroService = TestBed.get(HeroService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('createHero()', () => {
    const heroName = createHeroMock.name;
    heroService.createHero(heroName).subscribe((res: IHero) => {
      expect(res.name).toEqual(heroName);
      expect(res.name).toEqual(heroName);
    });

    const req = httpMock.expectOne(`${environment.apiUrl}/${heroService.basePath}`);
    expect(req.request.method).toEqual('POST');
    req.flush(createHeroMock);

    httpMock.verify();
  });


  it('getHero()', () => {
    const heroID = getHeroMock._id;
    heroService.getHero(heroID).subscribe((res: IHero) => {
      expect(res._id).toEqual(heroID);
    });

    const req = httpMock.expectOne(`${environment.apiUrl}/${heroService.basePath}/${heroID}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getHeroMock);

    httpMock.verify();
  });

  it('getHeroes()', () => {
    heroService.getHeroes().subscribe((res: IHero[]) => {
      expect(res.length).toBe(3);
    });

    const req = httpMock.expectOne(`${environment.apiUrl}/${heroService.basePath}`);
    expect(req.request.method).toEqual('GET');
    req.flush(getHeroesMock);

    httpMock.verify();
  });
});
