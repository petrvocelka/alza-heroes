import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-button-submit',
  templateUrl: './button-submit.component.html',
  styleUrls: ['./button-submit.component.scss']
})
export class ButtonSubmitComponent implements OnInit {

  @Input() isLoading = false;
  @Input() text: string;
  @Input() theme: 'blue' | 'red' = 'blue';

  @Output() clicked = new EventEmitter<any>();

  constructor() { }

  ngOnInit() {}

  onClick() {
    this.clicked.emit();
  }

  get themeClass() {
    return (this.theme) ? `button-submit--theme-${this.theme}` : '';
  }
}
