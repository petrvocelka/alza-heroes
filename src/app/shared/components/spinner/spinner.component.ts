import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-spinner',
  templateUrl: './spinner.component.html',
  styleUrls: ['./spinner.component.scss']
})
export class SpinnerComponent implements OnInit {

  @Input() isInline = false;
  @Input() isSmall = false;
  @Input() theme: 'blue' | 'red' | 'transparent' = 'blue';

  constructor() { }

  ngOnInit() {}

  get themeClass() {
    return (this.theme) ? `spinner--theme-${this.theme}` : '';
  }
}
